import sys 

file = sys.argv[1]
des = sys.argv[2]
des = open(des, 'wt')
last_name = ''
cn = 0 

for line in open(file):
    line = line.split()
    name, num = line[0], int(line[-1])
    if name == last_name:
        cn += num 
    else:
        line = last_name + '\t' + str(cn) + '\n'
        last_name = name
        if cn>0:
            des.write(line) 
        cn = num 

des.write(name + '\t' + str(cn) + '\n')
des.close()